package utils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;

public class TestHelper {
	public static WebDriver setUpFirefoxDriver() {
		System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")+"\\src\\test\\resources\\geckodriver.exe");
		FirefoxOptions ffOptions = new FirefoxOptions();
		FirefoxProfile profile = new FirefoxProfile();
		profile.setPreference("geo.enabled", false);
		profile.setPreference("geo.provider.use_corelocation", false);
		profile.setPreference("intl.accept_languages", "en");
		ffOptions.setCapability(FirefoxDriver.PROFILE, profile);
		FirefoxDriver driver = new FirefoxDriver(ffOptions);
		driver.manage().deleteAllCookies();
		driver.manage().window().setSize(new Dimension(1920, 1080));
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		return driver;
	}
	public static void takeScreenshot(String className, String methodName, WebDriver driver) {
		var num = 1;
		for (var winHandle : driver.getWindowHandles()) {
		      driver.switchTo().window(winHandle);
		      File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		      String filename = new SimpleDateFormat("yyyyMMddhhmmss'.png'").format(new Date());
		      File dest = new File("target/archive/screenshot/" + methodName + num + filename);
		      try {
		          FileUtils.copyFile(scrFile, dest);
		        } catch (IOException e1) {
		          // TODO Auto-generated catch block
		          e1.printStackTrace();
		        }
		        num = num + 1;
		}
	}
}
