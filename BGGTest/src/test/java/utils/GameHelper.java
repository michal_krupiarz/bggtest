package utils;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import restClient.GameService;

public class GameHelper {
	
	private static final String POLL_LANGUAGE_DEPENDANCY = "//poll[@name='language_dependence']/results/result";
	
	
	public static List<String> getMostVotedLanguageDependance(String id) {
		  return getMaxValuedKeysFromMap(getGameLanguageDependances(id));
	}
	
	private static Map<String,Integer> getGameLanguageDependances(String id){
		var gameInfo = GameService.getGameInfo(id);
		Map<String, Integer> gameLanguageDependances = new HashMap<>();		
		InputSource inputXML = new InputSource( new StringReader( gameInfo.asString() ) );
	    XPath xPath = XPathFactory.newInstance().newXPath();
		   
	       try {
			NodeList nodes =  (NodeList) xPath.evaluate(POLL_LANGUAGE_DEPENDANCY, inputXML,XPathConstants.NODESET); 
			if (nodes.getLength()<1)
				throw new RuntimeException("Xml has no searched nodes. Nodes searched: " +POLL_LANGUAGE_DEPENDANCY);
			for (int i = 0; i < nodes.getLength(); i++) {
					gameLanguageDependances.put(nodes.item(i)
								.getAttributes()
								.getNamedItem("value")
								.getNodeValue()
							, Integer.parseInt(nodes.item(i)
										.getAttributes()
										.getNamedItem("numvotes")
										.getNodeValue()));	        	
		        }

		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		
		return gameLanguageDependances;
	}
	private static List<String> getMaxValuedKeysFromMap(Map<String,Integer> map){
		List<String> maxValueKeysFromMap = new ArrayList<>();
		
		var maxValue = Collections.max(map.values());
		
		if (maxValue == 0) {
			maxValueKeysFromMap.add("(no votes)");
		} else {
		maxValueKeysFromMap = map.entrySet()
				.stream()
				.filter(item -> item.getValue() == maxValue)
				.map(Map.Entry::getKey)
				.collect(Collectors.toList());
		}
		
		return maxValueKeysFromMap;
	}
}
