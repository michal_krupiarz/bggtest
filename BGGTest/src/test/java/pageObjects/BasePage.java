package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {
	private static final int TIMEOUT_SECONDS = 15;
	private static final int TIMEOUT_MILISECONDS = TIMEOUT_SECONDS*1000;
	private static final int INTERVAL = 500;
	protected WebDriver driver;
	private WebDriverWait waitDriver;

	public BasePage(WebDriver driver) {
		this.driver = driver;
		waitDriver = new WebDriverWait(driver, TIMEOUT_SECONDS);		
	}
	
	protected void isElementVisible(By by) {
		waitForElementToBeVisible(by);	
	}
	
	private void waitForElementToBeVisible(By by) {
		waitDriver.until(ExpectedConditions.visibilityOfElementLocated(by));
	}
	
	protected void clickElement(By by) {
		waitForElementToBeClickable(by);
		scrollToElement(by);
		tryToClickElement(by);
	}
	
	private void waitForElementToBeClickable(By by) {
		waitDriver.until(ExpectedConditions.elementToBeClickable(by));
	}
	
	protected void isElementInvisible(By by) {
		waitForElementToBeInvisible(by);
	}
	
	private void waitForElementToBeInvisible(By by) {
		waitDriver.until(ExpectedConditions.invisibilityOfElementLocated(by));
	}
	protected void putTextIntoElement(By by, String text) {
		waitForElementToBeVisible(by);
		scrollToElement(by);
		driver.findElement(by)
			.sendKeys(text);
	}
	protected String getTextFromElementFromSetOfElements(By by, int numberOfElement) {
		isWebElementVisible(driver.findElements(by).get(numberOfElement));
		return driver.findElements(by).get(numberOfElement).getText();	
	}
	private void isWebElementVisible(WebElement el) {
		waitDriver.until(ExpectedConditions.visibilityOf(el));
	}
	protected String getTextFromElement(By by) {
		isElementVisible(by);
		scrollToElement(by);
		return driver.findElement(by).getText();
	}
	protected String getAttributeOfElement(By by, String attr) {
		isElementVisible(by);
		scrollToElement(by);
		return driver.findElement(by)
					.getAttribute(attr);
	}
	protected void scrollToElement(By by) {
	    WebElement element = driver.findElements(by).get(0);
	    ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
	    isElementVisible(by);
	}
	private void tryToClickElement(By by) {
		 boolean isObscured = true;
		 String obscureException = null;
		    var endTime = System.currentTimeMillis()+TIMEOUT_MILISECONDS;
		   
		    while (isObscured && System.currentTimeMillis()<endTime) {
		      try {
		        driver.findElement(by).click();
		        isObscured = false;
		      } catch(org.openqa.selenium.ElementClickInterceptedException e) {
		        isObscured = true;
		        obscureException = e.getMessage();
		      }
		      try {
		        Thread.sleep(50);
		      } catch (InterruptedException e) {
		        e.printStackTrace();
		      }
		    }
		    if (isObscured) {
				throw new ElementClickInterceptedException("Element " + by + " is obscured. Full exception: " + obscureException);
		      }
	}
}
