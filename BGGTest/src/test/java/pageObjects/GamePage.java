package pageObjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class GamePage extends BasePage{

	private static final String H1_A = "//h1/a";
	private static final String LANGUAGEDEPENDENCE_SPAN = "//span[@item-poll-button='languagedependence']/span";
	private static final String A_OVERVIEW = "//a[text()[normalize-space()='Overview']]";

	public GamePage(WebDriver driver) {
		super(driver);
		isElementVisible(By.xpath(A_OVERVIEW));
	}
	public GamePage isCorrectGameShown(String gameName) {
		Assert.assertEquals(getTextFromElement(By.xpath(H1_A)).trim(), gameName
				,"Page for wrong name loaded. Expected: " +gameName+ " got: " +getTextFromElement(By.xpath(H1_A)).trim());
		return this;
	}
	public GamePage isCorrectLanguageDependancyShow(List<String> mostVotedLanguageDependance) {	
		
		Assert.assertTrue(mostVotedLanguageDependance
				.contains(getTextFromElement(By.xpath(LANGUAGEDEPENDENCE_SPAN))),
				"List of most voted language dependancies: " +mostVotedLanguageDependance
				+ " does not cointain found language dependancy: " 
				+ getTextFromElement(By.xpath(LANGUAGEDEPENDENCE_SPAN)));
		return this;
	}
}
