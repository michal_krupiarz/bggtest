package pageObjects;

import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CollectionPage extends BasePage{

	private static final String SPAN_CLASS_GEEKPAGES = "//span[@class='geekpages']";
	private static final String TABLE_ID_COLLECTIONITEMS_GAME = "//table[@id='collectionitems']//tr[%s]/td[1]//a";
	private static final String DIV_BOARD_GAME_COLLECTION = "//div[@id='collection']/div[text()[contains(.,'Board Game Collection')]]";

	public CollectionPage(WebDriver driver) {
		super(driver);
		isElementVisible(By.xpath(DIV_BOARD_GAME_COLLECTION));
	}

	public GamePage clickGame(int numOfGame) {
		clickElement(By.xpath(String.format(TABLE_ID_COLLECTIONITEMS_GAME, numOfGame+1)));
		return new GamePage(driver);
	}

	public int getRandomGameNumber() {
		return new Random().nextInt(getNumberOfGamesInCollection()) +1;
	}

	public String getGameIDNumber(int randomGameNumber) {
		var fullHref = getAttributeOfElement(By.xpath(String.format(TABLE_ID_COLLECTIONITEMS_GAME
				, randomGameNumber+1))
				, "href");
		var splitted = fullHref.split("/");
		return splitted[4];
	}
	public int getNumberOfGamesInCollection() {
		return Integer.parseInt(getTextFromElementFromSetOfElements(By.xpath(SPAN_CLASS_GEEKPAGES)
				,1)
				.split(" ")[2]);
	}
	public String getGameName(int num) {
		return getTextFromElement(By.xpath(String.format(TABLE_ID_COLLECTIONITEMS_GAME, num+1)));
	}
}
