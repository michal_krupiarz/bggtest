package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MainPage extends BasePage{

	private static final String LI_A_TEXT_COLLECTION = "//li/a[text()='Collection']";
	private static final String SPAN_USERNAME = "//span[normalize-space(text())='%s']";
	private static final String MODAL_BUTTON_TEXT_SIGN_IN = "//form//button[text()='Sign In']";
	private static final String INPUT_PASSWORD = "inputPassword";
	private static final String INPUT_USERNAME = "inputUsername";
	private static final String H4_SIGN_IN_MODAL_TITLE = "//h4[text()='Sign in']";
	private static final String URL = "https://boardgamegeek.com/";
	private static final String BUTTON_SIGN_IN = "//button[text()='Sign In']";
	public MainPage(WebDriver driver) {
		super(driver);	
	}
	public MainPage openBoardGameMainPage() {
		driver.get(URL);
		isElementVisible(By.xpath(BUTTON_SIGN_IN));
		return this;
	}
	public MainPage loginToBoardGames(String login, String password) {
		clickSignInButton().fillUserName(login)
			.fillPassword(password)
			.clickSignInOnModal();
		return this;
	}
	public MainPage clickSignInOnModal() {
		clickElement(By.xpath(MODAL_BUTTON_TEXT_SIGN_IN));
		isElementVisible(By.xpath(H4_SIGN_IN_MODAL_TITLE));
		return this;
	}
	public MainPage fillPassword(String password) {
		putTextIntoElement(By.id(INPUT_PASSWORD),password);
		return this;
	}
	public MainPage fillUserName(String login) {
		putTextIntoElement(By.id(INPUT_USERNAME),login);
		return this;
	}
	public MainPage clickSignInButton() {
		clickElement(By.xpath(BUTTON_SIGN_IN));
		isElementVisible(By.xpath(H4_SIGN_IN_MODAL_TITLE));
		return this;
	}
	public MainPage isUserLoggedIn(String login) {
		isElementInvisible(By.xpath(String.format(SPAN_USERNAME, login)));		
		return this;
	}
	public MainPage clickUserMenu(String login) {
		clickElement(By.xpath(String.format(SPAN_USERNAME, login)));
		return this;
	}
	public CollectionPage clickCollectionInUserMenu() {
		clickElement(By.xpath(LI_A_TEXT_COLLECTION));
		return new CollectionPage(driver);
	}
}
