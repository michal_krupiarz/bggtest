package bGGTests;

import org.testng.annotations.Test;
import pageObjects.CollectionPage;
import pageObjects.MainPage;
import utils.GameHelper;
import utils.TestHelper;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;

public class BGGTestOne {
	
  private static final String LOGIN = "michalkrupiarz";
  private static final String PASSWORD = "3edcFvgy7";
  private WebDriver driver;
  
  @BeforeTest
  public void beforeTest() {
	  driver = TestHelper.setUpFirefoxDriver();
  }

  @AfterMethod
  public void afterTest(ITestResult result) {
	  if(!result.isSuccess()) {
		  TestHelper.takeScreenshot(getClass().getName()
				  , result.getName()
				  		.toString()
				  		.trim()
				  , driver);
	  }
	  driver.quit();
  }
  /**
   * 
   * Steps 
   * 1 Login to https://boardgamegeek.com
   * 2 Open user menu
   * 3 Click collection of games for logged in user 
   * 4 On collection page click random game
   * 5 Fetch via rest poll results about Language Dependance
   * 6 Check that most voted language dependance is presented on game page
   * 
   */
  @Test
  public void languageDependanceTest() {
	  MainPage mP = new MainPage(driver);
	  
	  CollectionPage cP = mP.openBoardGameMainPage()
	  	.loginToBoardGames(LOGIN, PASSWORD)
	  	.isUserLoggedIn(LOGIN)
	  	.clickUserMenu(LOGIN)
	  	.clickCollectionInUserMenu();
	  
	  var gameNumber = cP.getRandomGameNumber();
	  var gameName = cP.getGameName(gameNumber);
	  var gameID = cP.getGameIDNumber(gameNumber);
	  
	  cP.clickGame(gameNumber)
	  	.isCorrectGameShown(gameName)
	  	.isCorrectLanguageDependancyShow(GameHelper.getMostVotedLanguageDependance(gameID)); 
  }
}
