package stepDefinitions;

import org.openqa.selenium.WebDriver;

import io.cucumber.core.api.Scenario;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.CollectionPage;
import pageObjects.GamePage;
import pageObjects.MainPage;
import utils.GameHelper;
import utils.TestHelper;

public class StepDefinitionsBoardGameGeek {
	
	private WebDriver driver;
	private int gameNumber;
	private String gameID;
	private String gameName;
	
	@Before
	public void before() {
		driver = TestHelper.setUpFirefoxDriver();
	}
	
	@After
	public void after(Scenario scenario) {
		if(scenario.isFailed()) {
			TestHelper.takeScreenshot(getClass().getName()
					, scenario.getName()
					, driver);
		}
		driver.quit();
	}
	
	@Given("^Open game collection of user with \"(.*)\" \"(.*)\"$")
	public void openCollectionOfUser(String login, String password) {
		MainPage mP = new MainPage(driver);
		
		mP.openBoardGameMainPage()
		  .loginToBoardGames(login, password)
		  .isUserLoggedIn(login)
		  .clickUserMenu(login)
		  .clickCollectionInUserMenu();
	}
	@When("^Pick random game from collection$")
	public void pickRanodmGame() {
		CollectionPage cP = new CollectionPage(driver);
		 gameNumber = cP.getRandomGameNumber();
		 gameName = cP.getGameName(gameNumber);
		 gameID = cP.getGameIDNumber(gameNumber);
		 
		 cP.clickGame(gameNumber);
	}
	
	@Then("^Language dependancy presented should be correct with rest data$")
	public void compareLanugageDependancy() {
		GamePage gP = new GamePage(driver);
		gP.isCorrectGameShown(gameName)
	  	  .isCorrectLanguageDependancyShow(GameHelper.getMostVotedLanguageDependance(gameID)); 
	}
}
