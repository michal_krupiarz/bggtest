package restClient;

import io.restassured.response.Response;

public class GameService extends BaseRest{
	
	private static final String GAME_SERVICE = "/thing?id=";
	
	public static Response getGameInfo(String id) {
		return getRequest(GAME_SERVICE,id);
	}
}
