package restClient;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class BaseRest {
	private static final String BASE_URI = "https://www.boardgamegeek.com/xmlapi2";
	private static final String URI = "%s%s%s";
	
	private static String generateUri(String service, String param) {
		return String.format(URI,BASE_URI,service,param);
	}
	protected static Response getRequest(String service, String param) {
		RequestSpecification httpRequest = RestAssured.given();
		Response response;
		
		try {
			response = httpRequest.get(generateUri(service, param));
		} catch(Exception e) {
			System.out.println(e);
			throw new RuntimeException("Failed get request. Uri requested: " +generateUri(service, param));
		}
		
		if (response.getStatusCode()!=200) 
			throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatusCode()
					+ " Uri requested: "
					+ generateUri(service, param));
		
		return response;
	}
}
